FROM pyhf/pyhf:v0.5.4

## ensure locale is set during build
ENV LANG C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN python -m pip install --upgrade pip

RUN pip3 install torch==1.2.0 && \
    pip3 install jax==0.2.4 && \
    pip3 install jaxlib==0.1.56 && \
    pip3 install uproot && \
    pip3 install jupyter && \
    pip3 install jupyterhub && \
    pip3 install jupyterlab && \
    pip3 install matplotlib && \
    pip3 install seaborn && \
    pip3 install hep_ml && \
    pip3 install sklearn && \
    pip3 install tables && \
    pip3 install pydot && \
    pip3 install pyparser && \
    pip3 install pyparsing && \
    pip3 install pytest && \
    pip3 install pandas && \
    pip3 install pyro-ppl && \
    pip3 install tdqm && \
    pip3 install iminuit==1.5.3 && \
    pip3 install awkward && \
    pip3 install sympy


RUN apt-get update && \
    apt-get install -y git debconf-utils && \
    echo "krb5-config krb5-config/add_servers_realm string CERN.CH" | debconf-set-selections && \
    echo "krb5-config krb5-config/default_realm string CERN.CH" | debconf-set-selections && \
    apt-get install -y krb5-user && \
    apt-get install -y vim less screen graphviz python3-tk wget && \
    apt-get install -y jq tree hdf5-tools bash-completion
